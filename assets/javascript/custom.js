var url;
var myIndex = 0;
var showHeaderAt = 150;
var win = $(window),
    body = $('body');

$(document).ready(function() {
    $("header").load("/assets/html/header.html", function() {
        $("footer").load('/assets/html/footer.html', function() {
            $("#loader").remove();
            setInterval(function() {
                router();
            }, 50);

        })
    });

    // Show the fixed header only on larger screen devices

    if (win.width() > 400) {

        // When we scroll more than 150px down, we set the
        // "fixed" class on the body element.

        win.on('scroll', function(e) {

            if (win.scrollTop() > showHeaderAt) {
                body.addClass('fixed');
            } else {
                body.removeClass('fixed');
            }
        });
    }
})

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {
        myIndex = 1
    }
    x[myIndex - 1].style.display = "block";
    setTimeout(carousel, 2000); // Change image every 2 seconds
}

function router() {
    var nurl = window.location.href.toLowerCase();
    if (url !== nurl) {
        var x = window.location.pathname.toLowerCase().slice(1);
        $(".single_page").hide();
        console.log('tejal')
        if (x == '') {
            route('home');
        } else if (x == 'home') {
            showhome();
        } else if (x == 'profile') {
            showprofile();
        } else if (x == 'connect') {
            showconnection();
        } else if (x == 'contact') {
            showcontact();
        } else if (x == 'interest') {
            showinterest();
        } else if (x == 'error') {
            showerror();
        } else {
            show_pagenotfound();
        }
        url = nurl;
        $('body').attr('current_page', x);
    }
}

function route(x) {
    if (x !== window.location.pathname.toLowerCase().slice(1)) {
        history.pushState(null, "", "/" + x);
    }
}

function hideall() {
    $("#homepage").hide();
    $("#profilepage").hide();
    $("#connectionpage").hide();
    $("#contactpage").hide();
    $("#interestpage").hide();
    $("#errorpage").hide();
}


function showhome() {
    hideall();
    if ($("#homepage").html() == '') {
        $("#homepage").load("/assets/html/home.html", function() {
            $("#homepage").fadeIn();
            carousel();
        })
    } else {
        $("#homepage").fadeIn();
    }
}

function showprofile() {
    hideall();
    if ($("#profilepage").html() == '') {
        $("#profilepage").load("/assets/html/profile.html", function() {
            $("#profilepage").fadeIn();
            $(".footer").show();
        })
    } else {
        $("#profilepage").fadeIn();
    }
}

function showconnection() {
    hideall();
    if ($("#connectionpage").html() == '') {
        $("#connectionpage").load("/assets/html/connect.html", function() {
            $("#connectionpage").fadeIn();
            $('#dt').bootstrapMaterialDatePicker({
                    weekStart: 0,
                    format: 'hh:mm:ss',
                    date: false
                })
                /* $(".options").click(function(e) {
                    $(".desc_wrapper .desc").hide();
                    $(".desc_wrapper .desc:eq(0)").show();
                    console.log(e.target);
                }); */
        })
    } else {
        $("#connectionpage").fadeIn();
    }
}

function toggle1(ref) {
    $('.connect2 .active').removeClass('active');
    $(ref).addClass('active');
    $(".desc").hide();
    $(`.desc:eq(${$(ref).index()})`).show();
}

function showcontact() {
    hideall();
    $(".table_overlay").fadeOut();
    if ($("#contactpage").html() == '') {
        $("#contactpage").load("/assets/html/contact.html", function() {
            $("#contactpage").fadeIn();
            $('#sph_quantity').select2({
                placeholder: 'Select a month'
            });
        })
    } else {
        $("#contactpage").fadeIn();
    }
}

function showinterest() {
    hideall();
    if ($("#interestpage").html() == '') {
        $("#interestpage").load("/assets/html/interest.html", function() {
            $("#interestpage").fadeIn();
        })
    } else {
        $("#interestpage").fadeIn();
    }
}

function showerror() {
    hideall();
    if ($("#errorpage").html() == '') {
        $("#errorpage").load("/assets/html/error.html", function() {
            $("#errorpage").fadeIn();
            $(".header").hide();
            $(".footer").hide();
        })
    } else {
        $("#errorpage").fadeIn();
    }
}


function contactint() {
    let payload = {};
    let error = false;
    payload.name = $('#cf_name').val() ? $('#cf_name').val() : (error = true, console.log('name is empty'));
    payload.email = $('#cf_email').val() ? $('#cf_email').val() : (error = true, console.log('email is empty'));
    payload.number = $('#cf_number').val() ? $('#cf_number').val() : (error = true, console.log('number is empty'));
    payload.prof = $('#sph_quantity').val() ? $('#sph_quantity').val() : (error = true, console.log('professional is empty'));
    payload.query = $('#cf_msg').val() ? $('#cf_msg').val() : (error = true, console.log('query is empty'));
    /*  payload.sites = $('#linkchk').val() ? $('#linkchk').val() : (error = true, console.log('checkbox is not checked')); */
    showsnackbar('Please enter all the fields');
    if (!error) {
        $.ajax({
            url: 'services/contact.php',
            type: "POST",
            contentType: 'application/json',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(payload),
            success: function(response) {
                if (response.status == "success") {
                    $('#cf_name').val('');
                    $('#cf_email').val('');
                    $('#cf_number').val('');
                    $('#sph_quantity').val('');
                    $('#cf_msg').val('');
                    /*  $('#linkchk').val(''); */
                    showsnackbar("Details added succesfully");
                    console.log('Boss!Tt\'s done!');
                } else {
                    console.log('failed');
                }
            }

        })
    }
}

function contactfetch() {
    $.ajax({
                url: 'services/fetch.php',
                type: "GET",
                contentType: 'application/json',
                success: function(response) {
                        if (response.status == "success") {
                            $('#contact_fet').DataTable().destroy();
                            $('#contact_fet tbody').empty();
                            $.each(response.details, function(index, eachrow) {
                                        $('#contact_fet tbody').append(`<tr><td>${eachrow.id}</td><td>${eachrow.name}</td><td>${eachrow.email}</td><td>${eachrow.number}</td><td>${eachrow.prof}</td><td>${eachrow.query}</td><td>${`<span onclick="contactdelete(${eachrow.id})" style="color:red;cursor:pointer;">Delete</span>`}</td></tr>`);
                })
                $('#contact_fet').DataTable({
                    responsive: true,
                    searching: false,
                    paging: false
                });
                $(".table_overlay").fadeIn();
            } else {
                console.log(response.details);
            }
        }
    })
    
}

function contactdelete(x){
let payload={};
let error=false;
payload.id=x;
if(!error){
    $.ajax({
        url: 'services/delete.php',
        type: "POST",
        contentType: 'application/json',
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(payload),
        success: function(response) {
        if (response.status == "success") {
contactfetch();
        console.log('Boss!It\'s done!'); 
    } else {
        console.log('failed');  
    }
}
    })
}
}

function id_fetch(y){
    let payload={};
    payload.id=y;
    $.ajax({
        url: 'services/update.php',
        type: "POST",
        contentType: 'application/json',
        headers:{
'Content-Type':'application/json'
        },
        data: JSON.stringify(payload),
        success: function(response){
            if(response.status=="success"){
            $('#cf_name').val(response.details.name);
            $('#cf_email').val(response.details.email);
            $('#cf_number').val(response.details.number);
            $('#sph_quantity').val(response.details.prof);
            $('#cf_msg').val(response.details.query);
console.log('Done');
            }else{
                console.log('failed');
            }
        }
    })
}
function showsnackbar(x) {
    Snackbar.show({
        pos: 'bottom-center',
        showAction: false,
        text: x
    });
}

function contactupdate(){
    let payload={};
    payload.id=$('#idtext').val();
    payload.name=$('#cf_name').val();
    payload.email=$('#cf_email').val();
payload.number=$('#cf_number').val();
payload.prof=$('#sph_quantity').val();
payload.query=$('#cf_msg').val();
   $.ajax({
       url:'services/update_data.php',
       type:"POST",
       contentType:'application/json',
       headers:{
           'Content-Type':'application/json'
       },
       data: JSON.stringify(payload),
       success: function(response){
if(response.status=="success"){
    $('#cf_name').val('');
    $('#cf_email').val('');
    $('#cf_number').val('');
    $('#sph_quantity').val('');
    $('#cf_msg').val('');
    $('#idtext').val('');
    console.log('Done update');
}
else{
    console.log('failed');
}
       }
   })

}