<?php
date_default_timezone_set("Asia/Kolkata");
echo header('Content-Type:application/json');
include('dbconnection.php');
$array=array(); 
$conn=getConnection();
$result=mysqli_query($conn,"SELECT * from contact");
if($result){
    if(mysqli_num_rows($result)>0){
        while($row=mysqli_fetch_object($result)){
         $array[] = $row;
        }
        echo json_encode(array("status"=>"success","details"=>$array));
    }else {
        echo json_encode(array("status"=>"success","details"=>$result));
} 
}else{
    echo json_encode(array("status"=>"failed","details"=>$conn->error));
}
?>